import Ember from 'ember';

// returns the properie length of a object
export default Ember.Helper.helper(([obj, index]) => {
  if (obj == null)
    return null;

  return obj[index];
});
