import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'), // name
  sectiontype: DS.attr('string', {
    default: 'NORMAL'
  }), // metadata, normal
  website: DS.belongsTo('website', { async: false }),
  elements: DS.hasMany('element', { async: false }),
  timestamp: DS.attr('date'),
  isNewsPage: DS.attr('boolean', {
    defaultValue: false
  }),
  isFileUsage: DS.attr('boolean', {
    defaultValue: false
  }),
  account: DS.belongsTo('account', {
    async: false
  }),
  news: DS.hasMany('news', {
    async: false
  }),

  newsSortingDesc: ['timestamp:desc'],
  descNews: Ember.computed.sort('news', 'newsSortingDesc')
});
