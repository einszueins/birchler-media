import DS from 'ember-data';
import Ember from 'ember';

export default DS.Transform.extend({
  deserialize(serialized) {
    if (Ember.isEmpty(serialized)) {
      return {};
    }

    // TODO: fix. sometimes JSON.parse works some times not
    // sometimes we need the parse and sometimes its not needed to "make" a object.
    try {
      serialized = JSON.parse(serialized);
    } catch (e) {
      // console.log('FAILED with :');
      // console.log(e);
      // console.log('tried:');
      // console.log(serialized);
    }

    return serialized;
  },
  serialize(deserialized) {
    if (Ember.isEmpty(deserialized))
      return {};

    return deserialized;
  }
});
