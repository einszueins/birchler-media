import DS from 'ember-data';

export default DS.Model.extend({
  port: DS.attr('number'),
  pass: DS.attr('string'),
  host: DS.attr('string'),
  user: DS.attr('string'),
  website: DS.belongsTo('website', { async: false }),
  account: DS.belongsTo('account', {
    async: false
  })
});
