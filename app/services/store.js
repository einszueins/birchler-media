import DS from 'ember-data';
import Ember from 'ember';
import ENV from '../config/environment';

export default DS.Store.extend({
  sendToSprittClub(companyname, personnr, contactperson, tele, email, date, extra, testmail) {
    return this.sendToSpritt('CLUBREQUEST', null, tele, email, date, extra, companyname, personnr, contactperson, testmail);
  },

  sendToSprittPersonal(fullname, tele, email, date, extra, testmail) {
    return this.sendToSpritt('PERSONALREQUEST', fullname, tele, email, date, extra, null, null, null, testmail);
  },

  sendToSprittCompany(companyname, personnr, contactperson, tele, email, date, extra, testmail) {
    return this.sendToSpritt('COMPANYREQUEST', null, tele, email, date, extra, companyname, personnr, contactperson, testmail);
  },
  sendToSpritt(requesttype, fullname, tele, email, date, extra, companyname, personnr, contactperson, testmail) {
    return new Ember.RSVP.Promise(function(resolve, reject) {
      Ember.$.ajax({
        type: 'POST',
        url: ENV.api + '/publicmails/sprittmail',
        data: {
          requesttype,
          fullname,
          tele,
          email,
          date,
          extra,
          companyname,
          personnr,
          contactperson,
          testmail
        },
        dataType: 'json'
      }).then(function(success) {
        resolve(success);
      }, function(xhr, status, error) {
        reject(error);
      });
    });
  }

});
