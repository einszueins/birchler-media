import DS from 'ember-data';
import ENV from '../config/environment';

export default DS.RESTAdapter.extend({
  host: ENV.api,
  authorizer: 'authorizer:custom',

  shouldBackgroundReloadRecord: function() {
    // console.debug('background reload?', arguments);
    return false;
  }
});
