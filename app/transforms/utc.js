import DS from 'ember-data';

export default DS.Transform.extend({
  serialize(value) {
    return value ? value.toJSON() : null;
  },

  deserialize(value) {
    if (value)
      return moment.utc(value);

    return null;
  }
});
