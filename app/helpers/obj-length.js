import Ember from 'ember';

// returns the properie length of a object
export default Ember.Helper.helper(([obj]) => {
  if (obj == null)
    return null;

  return Object.keys(obj).length;
});
