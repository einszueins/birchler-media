import Ember from 'ember';

export default Ember.Helper.extend({
  compute: function(params) {
    let value = true;
    params.forEach(function(p) {
      if (!p)
        value = false;
    });

    return value;
  }
});
