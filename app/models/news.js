import DS from 'ember-data';

export default DS.Model.extend({
  headline: DS.attr('string'),
  textcontent: DS.attr('string'),
  filepath: DS.attr('string'),

  timestamp: DS.attr('date'),
  section: DS.belongsTo('section', {async: false}),
  website: DS.belongsTo('website', {async: false}),
  account: DS.belongsTo('account', {async: false })
});
