module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'birchler',
    environment: environment,
    // rootURL: './',
    // baseUrl: './',
    locationType: 'hash',
    // locationType: 'auto',

    EmberENV: {
      FEATURES: {
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
    }
  };

  if (environment === 'development') {
    ENV.api = 'http://localhost:3000/api'; // override
  }

  if (environment === 'test') {
    ENV.locationType = 'hash';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
    // rootURL= '/whatnthat/';
    ENV.locationType = 'hash';
  }

  // always direct on production
  ENV.api = 'http://einszueins.eu-central-1.elasticbeanstalk.com/api';

  return ENV;
};
