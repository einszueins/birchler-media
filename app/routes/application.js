import Ember from 'ember';

export default Ember.Route.extend({
  titleToken: 'Birchler Media',
  model() {
    let webpagetag = 14; // change this to the new webpage in the cms
    
    return Ember.RSVP.hash({
      website: this.store.query('website', {
        filter: JSON.stringify({
          where: {
            id: webpagetag
          },
          include: [{
            relation: 'sections',
            scope: {
              include: 'elements'
            }
          }]
        })
      })
    });
  }
});
