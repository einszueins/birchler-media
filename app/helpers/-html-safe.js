import Ember from 'ember';

export default Ember.Helpers.helper(([input]) => {
  return Ember.String.htmlSafe(input);
});
