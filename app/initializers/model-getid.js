import DS from 'ember-data';
import Ember from 'ember';

/**
* Adding a object.getId() to single model instances, returning the ID
*/
export default {
  name: 'model-getid',
  initialize() {
    DS.Model.reopen({
      getId(key) {
        // TODO(sn): support hasMany as well
        const rel = this._internalModel._relationships.initializedRelationships;

        return rel && rel[key] && rel[key].canonicalState && rel[key].canonicalState.id;
      },

      // https://github.com/emberjs/ember.js/issues/14014
      className: Ember.computed(function() {
        return this.get('constructor.modelName');
      })
    });
  }
};
