import Ember from 'ember';

export default Ember.Helper.helper(([input]) => {
  if (input)
    return input.string.replace(/<br\s*[\/]?>/gi, '');

  return null;
});
