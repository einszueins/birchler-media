import Ember from 'ember';

export default Ember.Helper.extend({
  /* jshint ignore:start*/
  /**
  * uses moment to generate a date string
  * @param {string} format, the date format to be returned
  * @param {moment} date, (otional) if not set --> date is now.
  * @param {int} dayOfWeek, (0-6)  (optional) //0 monday
  * @param {string} locale, For date language bug (optional)
  * @returns {string} representing a date.
  */
  /* jshint ignore:end */
  compute: function(param, {
    format, date, dayOfWeek, locale
  }) {

    let currDate;
    if (format == null)
      format = 'DD.MM.YYYY';

    if (Ember.isPresent(date)) {
      if (moment.isMoment(date)) {
        currDate = date.clone();
      } else {
        // currDate = moment.utc(date);
        // I think we only have non utc (e.g. timestamp) where we send in a non moment object
        currDate = moment(date);
      }
    } else {
      currDate = moment();
    }

    if (Ember.isPresent(locale)) {
      moment.locale(locale);
      currDate.locale(locale);
    } else {
      moment.locale('de');
    }

    if (Ember.isPresent(dayOfWeek)) {
      if (dayOfWeek === 0) {
        return currDate.startOf('isoWeek').format(format);
      } else if (dayOfWeek === 6) {
        return currDate.endOf('isoWeek').format(format);
      }
    } else {
      return currDate.format(format);
    }

    return null;
  }
});
