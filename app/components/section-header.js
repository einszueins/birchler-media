import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    scrollTo(to) {
      Ember.$('html, body').animate({
        scrollTop: Ember.$('#' + to).offset().top - 50
      }, 'slow');
    }
  }
});
