import Ember from 'ember';

export default Ember.Helper.extend({
  store: Ember.inject.service('store'),

  compute(params) {
    let store = this.get('store');

    return store.peekRecord('element', params[0]);
  }
});
