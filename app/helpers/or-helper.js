import Ember from 'ember';

export default Ember.Helper.extend({
  compute: function(params) {
    let found = false;
    params.forEach(function(p) {
      if (p)
        found = true;
    });

    if (found)
      return true;

    return false;
  }
});
