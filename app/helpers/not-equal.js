import Ember from 'ember';

export default Ember.Helper.helper(([lhs, rhs]) => {
  return lhs !== rhs;
});
