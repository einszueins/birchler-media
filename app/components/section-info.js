import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    scrollTo(to) {
      this.set('showFAQ', true);
      Ember.run.later(function() {
        Ember.$('html, body').animate({
          scrollTop: Ember.$('#' + to).offset().top - 50
        }, 'slow');
      }, 200);
    }
  }
});
