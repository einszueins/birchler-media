import Ember from 'ember';

export default Ember.Helper.helper(args => {
  // args is a sealed array and cant be returned instant
  let a = Ember.A();
  a.pushObjects(args);

  return a;
});
