import Ember from 'ember';

export default Ember.Controller.extend({
  init() {
    this._super(...arguments);
    let self = this;
    this.set('viewPortWith', Ember.$(window).outerWidth());
    Ember.$(window).resize(function() {
      self.set('viewPortWith', Ember.$(window).outerWidth());
    });

    // this.set('news', this.store.query('news', {
    //   filter: JSON.stringify({
    //     where: {
    //       websiteId: 13
    //     },
    //     order: 'timestamp DESC'
    //   })
    // }));

  },
  actions: {
    scrollTo(to) {
      Ember.$('html, body').animate({
        scrollTop: Ember.$('#' + to).offset().top - 50
      }, 'slow');
    }
  }
});
