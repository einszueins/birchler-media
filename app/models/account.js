import DS from 'ember-data';

export default DS.Model.extend({
  email: DS.attr('string'), // max 250 chars, required
  // lang: DS.attr('string'),
  username: DS.attr('string'),
  isAdmin: DS.attr('boolean'),
  timestamp: DS.attr('date'),
  websites: DS.hasMany('website', {
    async: false
  })
});
