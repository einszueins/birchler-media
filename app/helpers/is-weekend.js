import Ember from 'ember';

export default Ember.Helper.helper(([obj]) => {
  if (obj == null)
    return null;

  let theDay;
  if (obj === moment.isMoment(obj)) {
    theDay = obj;
  } else {
    // we got a string in e.g. 20160523
    theDay = new moment.utc(obj, 'YYYYMMDD');
  }

  if (theDay.day() === 6 || theDay.day() === 0)
    return true;

  return false;
});
