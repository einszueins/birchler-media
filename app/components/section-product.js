import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    gogoDetails(productId) {
      this.set('productId', productId);
      this.$('#detail-modal').modal('show');
    },
    gogoPrice(priceId) {
      this.set('priceId', priceId);
      this.$('#price-modal').modal('show');
    }
  }
});
