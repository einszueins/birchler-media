import DS from 'ember-data';
import _object from 'lodash/object';
import _lang from 'lodash/lang';

// Custom serializer to be compatible with loopback api server
//  - Normalization:
//    - prefix response payload with type
//    - extract included models recursively
//  - Serialization:
//    - omit the type on the root level
//    - suffix belongsTo relationships with 'Id'
export default DS.RESTSerializer.extend({
  normalizeResponse: function(store, primaryModelClass, payload, id, requestType) {
    let payload_ = {};
    payload_[primaryModelClass.modelName] = payload;

    return this._super(store, primaryModelClass, payload_, id, requestType);
  },
  extractRelationships: function(type, hash) {
    let relationships = {};
    type.eachRelationship((key, rel) => {
      let data;
      if (rel.kind === 'belongsTo') {
        if (hash[key]) {
          data = this.extractRelationship(rel.type, hash[key]);
        }
        // Fall back to <key>Id entry
        let keyId = key + 'Id';
        if (!data && hash[keyId]) {
          data = this.extractRelationship(rel.type, hash[keyId]);
        }
      } else if (rel.kind === 'hasMany' && hash[key]) {
        data = hash[key].map((item) => this.extractRelationship(rel.type, item)); // eslint-disable-line
      }
      if (data !== undefined) {
        relationships[key] = {
          data: data
        };
      }
    });

    return relationships;
  },
  extractRelationship: function(type, hash) {
    if (_lang.isObject(hash)) {
      let model = this.store.modelFor(type);
      let normalized = this.normalize(model, hash, '');
      let record = this.store.push(normalized);

      return {
        id: record.id,
        type: type
      };
    }

    return this._super(type, hash);
  },
  serializeIntoHash: function(hash, type, snapshot, options) {
    return _object.extend(hash, this.serialize(snapshot, options));
  },
  serializeAttribute(snapshot, json, key, attribute) {
    // do not serialize the attribute!
    if (attribute.options && attribute.options.readOnly) {
      return;
    }
    this._super(...arguments);
  },
  serializeBelongsTo: function(snapshot, json, relationship) {
    this._super(snapshot, json, relationship);
    const name = relationship.key;
    if (name.substring(name.length - 2) === 'Id') {
      return;
    }
    const nameId = name + 'Id';
    if (json[name] !== undefined && json[nameId] === undefined) {
      json[nameId] = json[name];
      delete json[name];
    }
  },
  normalizeDeleteRecordResponse(store, primaryModelClass, payload, id, requestType) {
    let payload_ = {};
    payload_[primaryModelClass.modelName] = {
      id: id
    };

    return this._super(store, primaryModelClass, payload_, id, requestType);
  },
  normalizeQueryRecordResponse(store, primaryModelClass, payload, id, requestType) {
    let payload_ = {};

    if (payload == null || payload[primaryModelClass.modelName] == null || payload[primaryModelClass.modelName].length === 0) {
      // console.log('no query response');
      payload_[primaryModelClass.modelName] = [];
      // this gives a null value as return result
    } else if (payload[primaryModelClass.modelName].length > 1) {
      // we fucked something up in the filter query
      // how to handle this?
      payload_ = payload;
      console.log('found a QueryRecord problem');
    } else {
      payload_[primaryModelClass.modelName] = payload[primaryModelClass.modelName][0];
    }

    return this._super(store, primaryModelClass, payload_, id, requestType);
  }
});
