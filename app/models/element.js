import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'), // name
  elementtype: DS.attr('string'), // FILE / TEXT / FILEARRAY / FILEARRAYELEMENT / MULTIDATA / MULIDATAELEMENT
  textcontent: DS.attr('string'),
  filepath: DS.attr('string'),
  multiData: DS.attr('object'),

  timestamp: DS.attr('date'),
  section: DS.belongsTo('section', {
    async: false
  }),
  website: DS.belongsTo('website', {
    async: false
  }),
  account: DS.belongsTo('account', {
    async: false
  }),
  parentElement: DS.belongsTo('element', {
    async: false,
    inverse: 'childs'
  }),
  sorting: DS.attr('number'),

  childs: DS.hasMany('element', {
    inverse: 'parentElement'
  }),

  childsAsArray: Ember.computed('childs', function() {
    return this.get('childs').toArray();
  }),

  childsorting: ['sorting'],
  sortedChilds: Ember.computed.sort('childs', 'childsorting')
});
