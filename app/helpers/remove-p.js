import Ember from 'ember';

export default Ember.Helper.helper(([input]) => {
  if (input)
    return input.string.replace(/<p\s*[\/]?>/gi, '');

  return null;
});
