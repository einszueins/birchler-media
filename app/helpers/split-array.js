import Ember from 'ember';

export default Ember.Helper.helper(([array, splitin]) => {
  if (array == null || splitin == null)
    return array;

  let length = array.get('length');
  let newArray = Ember.A();
  let a = Ember.A();
  for (let i = 0; i < length; i++) {
    if (i % splitin === 0 && i !== 0) {
      newArray.pushObject(a);
      a = Ember.A();
    }
    a.pushObject(array.objectAt(i));
  }

  newArray.pushObject(a);

  return newArray;
});
