import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'), // http:XX
  sections: DS.hasMany('section', {
    async: false
  }),
  timestamp: DS.attr('date'),
  account: DS.belongsTo('account', {
    async: false
  }),
  website: DS.belongsTo('filemanager', { async: false })
});
